package de.nurmavin.neraja.command.commands.developer;

import com.mewna.catnip.entity.util.Permission;
import de.nurmavin.neraja.command.*;

public final class TestCommand extends Command {
    public TestCommand() {
        super("test", CommandLevel.DEVELOPER);
    }

    @Override
    public void execute(CommandContext commandContext) {
        CommandExceptions.missingPermissions(commandContext, Permission.values());
    }
}