package de.nurmavin.neraja.command.commands.fun;

import com.mewna.catnip.entity.builder.EmbedBuilder;
import com.mewna.catnip.entity.guild.Member;
import com.mewna.catnip.entity.user.User;
import de.nurmavin.neraja.command.Command;
import de.nurmavin.neraja.command.CommandContext;
import de.nurmavin.neraja.command.CommandException;
import de.nurmavin.neraja.utils.Embeds;
import de.nurmavin.neraja.utils.UserRandom;
import gg.amy.catnip.utilities.FinderUtil;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

public final class RateCommand extends Command {
    public RateCommand() {
        super("rate", "ratewaifu");
    }

    @Override
    public void execute(CommandContext commandContext) throws CommandException {
        if(commandContext.hasArgs()) {
            List<Member> members =  new ArrayList<>(FinderUtil.findMembers(commandContext.arg(0).asString(),
                                                                           commandContext.guild()));

            if(members.size() > 0) {
                rateMember(commandContext, members.get(0));
            } else {
                Embeds.usage(commandContext);
            }
        } else {
            rateMember(commandContext, commandContext.member());
        }
    }

    private void rateMember(CommandContext commandContext, Member member) {
        Random random = new UserRandom(member.idAsLong());

        int rated = random.nextInt(10);

        if(member.id().equals("308051303895007232") || member.id().equals("562415519454461962")) rated = 11;

        commandContext.translate("command.rate.message", s -> commandContext.send(Embeds.normalEmbed(commandContext.member())
                                                                                        .description(s).build()), member.asMention(), rated);
    }
}
