package de.nurmavin.neraja.command;

import com.mewna.catnip.entity.util.Permission;
import de.nurmavin.neraja.translation.I18n;
import de.nurmavin.neraja.utils.Embeds;

import java.util.Arrays;
import java.util.stream.Collectors;

public final class CommandExceptions {
    public static void missingPermissions(CommandContext commandContext, Permission... permission) {
        I18n.translate(commandContext, "command.missingPermissions", s ->
                               Embeds.commandException(commandContext, new CommandException(s)),
                       Arrays.stream(permission).map(Permission::permName).collect(Collectors.joining(", ")));
    }
}
