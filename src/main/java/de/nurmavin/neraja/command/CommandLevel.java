package de.nurmavin.neraja.command;

public enum CommandLevel {
    DEVELOPER,
    ADMIN,
    MODERATOR,
    USER
}
