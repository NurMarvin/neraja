package de.nurmavin.neraja.command;

public final class CommandException extends Exception {
    public CommandException(String message) {
        super(message);
    }
}
