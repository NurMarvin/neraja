package de.nurmavin.neraja.utils;

import com.google.common.collect.Maps;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import com.mewna.catnip.entity.user.Presence;
import de.nurmavin.neraja.Neraja;

import java.awt.*;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

public class Settings {

    @SerializedName("debug")
    private boolean debug = false;

    @SerializedName("connection")
    private ConnectionSettings connectionSettings = new ConnectionSettings();

    @SerializedName("bot")
    private BotSettings botSettings = new BotSettings();

    @SerializedName("apiKeys")
    private HashMap<String, String> apiKeys = Maps.newHashMap();

    public class ConnectionSettings {
        @SerializedName("token")
        private String token = "";

        @SerializedName("mysqlHost")
        private String mysqlHost = "localhost";

        @SerializedName("mysqlDatabase")
        private String mysqlDatabase = "neraja";

        @SerializedName("mysqlUser")
        private String mysqlUser = "username";

        @SerializedName("mysqlPassword")
        private String mysqlPassword = "password";

        public String token() {
            return token;
        }

        public String mysqlHost() {
            return mysqlHost;
        }

        public String mysqlDatabase() {
            return mysqlDatabase;
        }

        public String mysqlUser() {
            return mysqlUser;
        }

        public String mysqlPassword() {
            return mysqlPassword;
        }
    }

    public class BotSettings {
        private String botName = "Neraja";

        private String owner = "562415519454461962";

        private String prefix = "-";

        private List<String> games = Arrays.asList("{prefix}help | {guilds} guilds", "{prefix}help | neraja.bot");

        private Presence.ActivityType activityType = Presence.ActivityType.PLAYING;

        @JsonAdapter(ColorTypeAdapter.class)
        private Color normalColor = new Color(255, 71, 87);

        @JsonAdapter(ColorTypeAdapter.class)
        private Color errorColor = new Color(231, 76, 60);

        public String botName() {
            return botName;
        }

        public String owner() {
            return owner;
        }

        public String prefix() {
            return prefix;
        }

        public List<String> games() {
            return games;
        }

        public Presence.ActivityType activityType() {
            return activityType;
        }

        public Color normalColor() {
            return normalColor;
        }

        public Color errorColor() {
            return errorColor;
        }
    }

    public static Settings instance() {
        return Neraja.instance().settings();
    }

    public ConnectionSettings connectionSettings() {
        return connectionSettings;
    }

    public BotSettings botSettings() {
        return botSettings;
    }

    public HashMap<String, String> apiKeys() {
        return apiKeys;
    }

    public boolean debug() {
        return debug;
    }

    public void debug(boolean debug) {
        this.debug = debug;
    }
}
