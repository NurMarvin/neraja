package de.nurmavin.neraja.utils;

import com.google.common.collect.Lists;
import com.google.common.util.concurrent.RateLimiter;
import com.mewna.catnip.entity.channel.GuildChannel;
import com.mewna.catnip.entity.guild.Guild;
import com.mewna.catnip.entity.message.Message;
import com.mewna.catnip.entity.util.Permission;
import de.nurmavin.neraja.Neraja;
import de.nurmavin.neraja.manager.impl.SimpleRaidManager;

import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class RaidedGuild {
    private String guildId;
    private int level;
    private List<Message> messages;
    private RateLimiter rateLimiter;
    private RateLimiter violations;

    public RaidedGuild(String guildId) {
        this.guildId = guildId;
        this.level = 0;
        this.messages = Lists.newArrayList();
        this.rateLimiter = RateLimiter.create(1, 10, TimeUnit.SECONDS);
        this.violations = RateLimiter.create(10, 10, TimeUnit.SECONDS);
    }

    public int level() {
        return level;
    }

    public void level(int level) {
        this.level = level;
    }

    public List<Message> messages() {
        return messages;
    }

    public void messages(List<Message> messages) {
        this.messages = messages;
    }

    public void handleNewMessage(Message message) {
        messages.add(message);

        Message lastMessage = latestMessage();

        if(message.content().startsWith("--force-level ")) {
            this.updateLevel(Integer.parseInt(message.content().split("--force-level ")[1]));
            return;
        }

        if(lastMessage == null || lastMessage.author().idAsLong() == message.idAsLong()) {
            if(lastMessage == null)
                return;
        }

        double messageDelay = lastMessage.timestamp().toInstant().until(message.timestamp(), ChronoUnit.MILLIS);

        if(messageDelay >= SimpleRaidManager.MIN_MESSAGE_DELAY_MS) return;

        if(!rateLimiter.tryAcquire()) {
            if(!violations.tryAcquire()) {
                this.updateLevel(this.level++);
            }
        }
    }

    public void updateLevel(int level) {
        System.out.println("Level: " + level);

        switch (level) {
            case 1: {
                Message message = latestMessage();

                message.catnip().rest().channel().modifyChannel(message.channelId(), new GuildChannel.ChannelEditFields()
                        .rateLimitPerUser(10)).thenAccept(guildChannel -> guildChannel.asTextChannel().sendMessage(
                        "Slowmode updated to 10s to prevent raid. Examining situation further..."));
                break;
            }
            case 2: {
                Neraja.instance().catnip().rest().guild().modifyGuild(guildId, new Guild.GuildEditFields()
                        .verificationLevel(Guild.VerificationLevel.LOW))
                      .thenAccept(guild -> latestMessage().channel().asTextChannel().sendMessage(
                              "Verification level bumped up to verified email. Examining situation further..."));
                break;
            }
            case 3: {
                Neraja.instance().catnip().rest().guild().modifyGuild(guildId, new Guild.GuildEditFields()
                        .verificationLevel(Guild.VerificationLevel.MEDIUM))
                      .thenAccept(guild -> latestMessage().channel().asTextChannel().sendMessage(
                              "Verification level bumped up to 5 minute registration time. Examining situation " +
                              "further..."));
                break;
            }
            case 4: {
                Neraja.instance().catnip().rest().guild().modifyGuild(guildId, new Guild.GuildEditFields()
                        .verificationLevel(Guild.VerificationLevel.HIGH))
                      .thenAccept(guild -> latestMessage().channel().asTextChannel().sendMessage(
                              "Verification level bumped up to 10 minute member time. Examining situation " +
                              "further..."));
                break;
            }
            case 5: {
                Neraja.instance().catnip().rest().guild().modifyGuild(guildId, new Guild.GuildEditFields()
                        .verificationLevel(Guild.VerificationLevel.VERY_HIGH))
                      .thenAccept(guild -> latestMessage().channel().asTextChannel().sendMessage(
                              "Verification level bumped up to phone verification. Examining situation " +
                              "further..."));
                break;
            }
            case 10: {
                Message message = latestMessage();
                message.channel().asTextChannel().sendMessage(
                        "All previous attempts to secure the channel failed. Locking channel...");

                Neraja.instance().catnip().rest().channel().modifyChannel(message.channelId(), new GuildChannel.ChannelEditFields()
                        .roleOverride(message.guildId(), permissionOverrideData ->
                                permissionOverrideData.deny(Permission.SEND_MESSAGES))).thenAccept(guildChannel -> {
                    guildChannel.asTextChannel().sendMessage("Channel locked.");
                }).exceptionally(throwable -> {
                    throwable.printStackTrace();
                    return null;
                });
                break;
            }
        }
    }

    public Message latestMessage() {
        if(messages.size() < 2) return null;
        return messages.get(messages.size() - 2);
    }
}