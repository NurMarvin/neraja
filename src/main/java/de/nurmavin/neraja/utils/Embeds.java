package de.nurmavin.neraja.utils;

import com.mewna.catnip.entity.builder.EmbedBuilder;
import com.mewna.catnip.entity.guild.Member;
import com.mewna.catnip.entity.user.User;
import com.mewna.catnip.util.CatnipMeta;
import de.nurmavin.neraja.Neraja;
import de.nurmavin.neraja.command.CommandContext;
import de.nurmavin.neraja.translation.I18n;
import org.apache.commons.lang3.exception.ExceptionUtils;

import java.awt.*;
import java.util.Set;

public final class Embeds {
    public static void usage(CommandContext context) {
        I18n.translate(context, context.command().description(), translatedDescription -> {
            StringBuilder description = new StringBuilder()
                    .append(translatedDescription)
                    .append("\n\n`")
                    .append(context.command().usage());

            context.command().subCommands().values().forEach(command -> description.append("\n").append(command.usage()));
            description.append("`");

            EmbedBuilder usage = errorEmbed(context.user())
                    .title("Command Usage: " + context.command().name())
                    .description(description.toString());
            context.send(usage.build());
        });
    }

    public static void commandException(CommandContext context, Exception e) {
        EmbedBuilder usage = errorEmbed(context.user())
                .description(e.getMessage());
        context.send(usage.build());
    }

    public static void exception(CommandContext context, Exception e) {
        String stackTrace = ExceptionUtils.getStackTrace(e);
        EmbedBuilder usage = errorEmbed(context.user())
                .description(stackTrace.substring(0, Math.min(2048, stackTrace.length())));
        context.send(usage.build());
    }

    public static EmbedBuilder errorEmbed(User sender) {
        return new EmbedBuilder()
                .color(Settings.instance().botSettings().errorColor())
                .title("Error")
                .footer(Neraja.instance().settings().botSettings().botName() + " " + Neraja.BOT_VERSION + " " +
                        "by NurMarvin#0420 running on Catnip " + CatnipMeta.VERSION, sender.effectiveAvatarUrl());
    }

    public static EmbedBuilder normalEmbed(Member member) {
        return new EmbedBuilder()
                .color(normalColor(member))
                .footer(Neraja.instance().settings().botSettings().botName() + " " + Neraja.BOT_VERSION + " " +
                        "by NurMarvin#0420 running on Catnip " + CatnipMeta.VERSION,
                        member.user().effectiveAvatarUrl());
    }

    public static Color normalColor(Member member) {
        return member != null && member.color() != null ? member.color() :
               Settings.instance().botSettings().normalColor();
    }
}
