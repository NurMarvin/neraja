package de.nurmavin.neraja.utils;

import java.util.Calendar;
import java.util.Random;

public class UserRandom extends Random {

    public UserRandom(long userId) {
        super(userId / 420 + (userId * 1337 % Calendar.getInstance().get(Calendar.DAY_OF_YEAR)));
    }

    public UserRandom(long userId, long secondUserId) {
        super(userId / 420 + (secondUserId * 1337 % Calendar.getInstance().get(Calendar.DAY_OF_YEAR)));
    }
}
