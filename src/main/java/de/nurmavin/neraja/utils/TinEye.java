package de.nurmavin.neraja.utils;

import okhttp3.*;

import java.io.IOException;
import java.util.concurrent.CompletableFuture;
import java.util.function.Consumer;

public class TinEye {
    public static void makeImageSearch(String imageUrl, Consumer<Response> onResponse,
                                       Consumer<IOException> onFailure) {
        OkHttpClient okHttpClient = new OkHttpClient();

        HttpUrl.Builder urlBuilder =
                HttpUrl.parse("https://tineye.com/search").newBuilder();
        String url = urlBuilder.build().toString();

        Request request = new Request.Builder()
                .header("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:66.0) Gecko/20100101 Firefox/66.0")
                .url(url)
                .post(RequestBody.create(MediaType.get("application/x-www-form-urlencoded"), "url=" + imageUrl))
                .build();

        okHttpClient.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                onFailure.accept(e);
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                onResponse.accept(response);
            }
        });
    }
}
