package de.nurmavin.neraja.profile.impl;

import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;
import com.mewna.catnip.entity.user.User;
import de.nurmavin.neraja.profile.UserSettings;
import de.nurmavin.neraja.translation.Language;

@DatabaseTable(tableName = "UserSettings")
public final class MySQLUserSettings implements UserSettings {
    @DatabaseField(id = true, unique = true)
    private long id;
    @DatabaseField(dataType = DataType.ENUM_TO_STRING)
    private Language language;

    /**
     * Used by OrmLite to create a new instance of {@link UserSettings} to then populate it
     */
    @SuppressWarnings("unused")
    public MySQLUserSettings() {
    }

    public MySQLUserSettings(User user) {
        this.id = user.idAsLong();
        this.language = Language.ENGLISH_US;
    }

    public long id() {
        return id;
    }

    public UserSettings id(long id) {
        this.id = id;
        return this;
    }

    public Language language() {
        return language;
    }

    public UserSettings language(Language language) {
        this.language = language;
        return this;
    }
}
