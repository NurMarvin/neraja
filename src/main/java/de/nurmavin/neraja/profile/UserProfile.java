package de.nurmavin.neraja.profile;

public interface UserProfile {
    String username();
    String discriminator();
    String avatarUrl();
    long id();
    long money();
    UserSettings userSettings();
    void userSettings(UserSettings userSettings);
}
