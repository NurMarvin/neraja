package de.nurmavin.neraja.profile;

import de.nurmavin.neraja.translation.Language;

public interface UserSettings {
    long id();
    Language language();
}
