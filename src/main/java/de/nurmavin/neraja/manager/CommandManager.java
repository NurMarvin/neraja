package de.nurmavin.neraja.manager;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.google.gson.annotations.JsonAdapter;
import com.mewna.catnip.entity.impl.MessageImpl;
import com.mewna.catnip.entity.message.Message;
import de.nurmavin.neraja.command.Command;
import de.nurmavin.neraja.manager.impl.SimpleCommandManager;
import org.apache.commons.lang3.NotImplementedException;

import java.util.Map;

@JsonDeserialize(as = SimpleCommandManager.class)
public interface CommandManager {
    default void handle(Message message) {
        throw new NotImplementedException("Command Manager does not implement CommandManager#handle");
    }

    Map<String, Command> commands();

    void registerCommand(Command command);
}
