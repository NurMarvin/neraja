package de.nurmavin.neraja.manager;

import com.github.benmanes.caffeine.cache.Cache;
import de.nurmavin.neraja.profile.UserSettings;

import java.util.function.Consumer;

public interface UserSettingsManager {
    UserSettings userSettingsById(long userId);

    void userSettingsByIdAsync(long userId, Consumer<UserSettings> userSettings);

    boolean hasSettings(long userId);

    Cache<Long, UserSettings> userSettingsCache();

    void upload(UserSettings userProfile);
}
