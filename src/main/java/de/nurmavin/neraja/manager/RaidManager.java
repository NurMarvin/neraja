package de.nurmavin.neraja.manager;

import com.mewna.catnip.entity.message.Message;

public interface RaidManager {
    void handle(Message message);

    void toggle();

    boolean state();
}
