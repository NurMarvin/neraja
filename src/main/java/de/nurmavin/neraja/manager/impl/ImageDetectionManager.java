package de.nurmavin.neraja.manager.impl;

import com.mewna.catnip.entity.message.Message;
import de.nurmavin.neraja.utils.TinEye;

import java.io.IOException;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public final class ImageDetectionManager {
    private static final Pattern TITLE_PATTERN = Pattern.compile(" <title>\\n(\\d{1,3}) results\\n - TinEye</title>");

    public void handle(Message message) {
        List<Message.Attachment> attachments = message.attachments().stream().filter(Message.Attachment::image)
                                                      .collect(Collectors.toList());

        attachments.forEach(attachment -> TinEye.makeImageSearch(attachment.url(), response -> {
            Matcher matcher = null;
            try {
                matcher = TITLE_PATTERN.matcher(response.body().string());
            } catch (IOException e) {
                e.printStackTrace();
            }

            if(matcher.find()) {
                Integer amountOfResults = Integer.valueOf(matcher.group(1));

                if(amountOfResults > 10) {
                    message.react(message.guild().emoji(567341551454388225L).forReaction());
                    message.channel().sendMessage("Definite catfish detected!");
                } else if (amountOfResults > 0) {
                    message.react("\uD83D\uDE05");
                    message.channel().sendMessage("Potential catfish detected!");
                } else {
                    message.react("\uD83D\uDC4D");
                    message.channel().sendMessage("All clear. Nothing to worry about");
                }
            }
        }, Throwable::printStackTrace));
    }
}
