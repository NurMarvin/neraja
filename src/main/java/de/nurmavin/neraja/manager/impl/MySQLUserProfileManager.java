package de.nurmavin.neraja.manager.impl;

import com.github.benmanes.caffeine.cache.Cache;
import com.github.benmanes.caffeine.cache.Caffeine;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.QueryBuilder;
import com.mewna.catnip.entity.user.User;
import de.nurmavin.neraja.Neraja;
import de.nurmavin.neraja.manager.UserProfileManager;
import de.nurmavin.neraja.profile.UserProfile;
import de.nurmavin.neraja.profile.UserSettings;
import de.nurmavin.neraja.profile.impl.MySQLUserProfile;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.SQLException;
import java.util.List;
import java.util.function.Consumer;

public final class MySQLUserProfileManager implements UserProfileManager {

    private static final Logger LOGGER = LoggerFactory.getLogger(UserProfileManager.class);

    private Dao<UserProfile, Long> userProfileDao;
    private Cache<Long, UserProfile> userProfileCache;
    private ThreadGroup threadGroup;

    public MySQLUserProfileManager() {
        this.userProfileDao = Neraja.instance().mySQLManager().createDao(MySQLUserProfile.class);
        this.userProfileCache = Caffeine.newBuilder().weakValues().maximumSize(1000).build();
        this.threadGroup = new ThreadGroup("UserProfile");
    }

    @Override
    public UserProfile userProfileById(long userId) {
        return this.userProfileCache.get(userId, ignored -> {
            QueryBuilder<UserProfile, Long> statementBuilder = this.userProfileDao.queryBuilder();
            try {
                statementBuilder.where().idEq(userId);
                List<UserProfile> userProfiles = this.userProfileDao.query(statementBuilder.prepare());
                UserProfile foundUserProfile;
                if(userProfiles.size() > 0) foundUserProfile = userProfiles.get(0);
                else {
                    User user = Neraja.instance().catnip().cache().user(userId);
                    if(user != null) {
                        foundUserProfile = new MySQLUserProfile(user);
                        this.upload(foundUserProfile);
                    }
                    else foundUserProfile = null;
                }

                if(foundUserProfile != null) {
                    foundUserProfile.userSettings(Neraja.instance().userSettingsManager().userSettingsById(foundUserProfile.id()));
                    this.userProfileCache.put(userId, foundUserProfile);
                }

                return foundUserProfile;
            }
            catch (Exception ex) {
                LOGGER.error("Error while searching for user profile for user id {}", userId, ex);
            }
            return null;
        });
    }

    @Override
    public void userProfileByIdAsync(long userId, Consumer<UserProfile> userProfile) {
        new Thread(threadGroup, () -> userProfile.accept(this.userProfileById(userId))).start();
    }

    @Override
    public boolean hasProfile(long userId) {
        return this.userProfileById(userId) == null;
    }

    @Override
    public Cache<Long, UserProfile> userProfileCache() {
        return userProfileCache;
    }

    @Override
    public void upload(UserProfile userProfile) {
        try {
            this.userProfileDao.createOrUpdate(userProfile);
            LOGGER.debug("Uploaded user profile for %s#%s (%s)", userProfile.username(),
                         userProfile.discriminator(), userProfile.id());
        } catch (SQLException e) {
            LOGGER.error(String.format("Error while uploading user profile for %s#%s (%s)", userProfile.username(),
                         userProfile.discriminator(), userProfile.id()), e);
        }
    }
}
