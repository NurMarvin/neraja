package de.nurmavin.neraja.manager.impl;

import com.google.common.base.CharMatcher;
import com.google.common.base.Joiner;
import com.google.common.base.Splitter;
import com.google.common.collect.Maps;
import com.mewna.catnip.entity.channel.Channel;
import com.mewna.catnip.entity.channel.MessageChannel;
import com.mewna.catnip.entity.message.Message;
import de.nurmavin.neraja.Neraja;
import de.nurmavin.neraja.command.Command;
import de.nurmavin.neraja.command.CommandContext;
import de.nurmavin.neraja.command.CommandException;
import de.nurmavin.neraja.manager.CommandManager;
import de.nurmavin.neraja.translation.TranslationNotFoundException;
import de.nurmavin.neraja.utils.Embeds;
import de.nurmavin.neraja.utils.Settings;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.Member;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

public final class SimpleCommandManager implements CommandManager {
    private static final Logger LOGGER = LoggerFactory.getLogger(CommandManager.class);
    private Map<String, Command> commandMap;

    public SimpleCommandManager() {
        this.commandMap = Maps.newHashMap();
    }

    @Override
    public void registerCommand(Command command) {
        if (command == null) return;

        List<String> aliases = Arrays.asList(command.aliases());

        commandMap.put(command.name().toLowerCase(), command);
        aliases.forEach(alias -> commandMap.put(alias, command));

        command.onRegister();
        LOGGER.info("Registered command: {} -> {}", command.name(), command);
    }

    @Override
    public void handle(Message message) {
        if (message.channel().type() != Channel.ChannelType.TEXT || !message.channel().isGuild()) return;

        Settings settings = Settings.instance();
        String content = message.content();

        if(content.startsWith(settings.botSettings().prefix())) {
            content = content.trim().substring(settings.botSettings().prefix().length()).trim();
            handle(message, content);
        }
    }

    private void handle(Message message, String content) {
        List<String> parts = Splitter.on(CharMatcher.breakingWhitespace()).splitToList(content);

        if(!parts.isEmpty()) {
            Command command = commandMap.get(parts.get(0).toLowerCase());

            if(command != null) {
                dispatch(createContext(parts, command, message));
            }
        }
    }

    private void dispatch(CommandContext context) {
        Message message = context.message();

        LOGGER.info("User {}#{} ({}) [{}({})/{}-{}] invoked command: {}",
                 message.author().username(), message.author().discriminator(),
                 message.author().id(), context.guild().name(), message.guildId(),
                 message.channel().id(), message.id(), message.content());

        try {
            context.command().preExecute(context);
        }
        catch (CommandException | TranslationNotFoundException e) {
            Embeds.commandException(context, e);
        } catch (Exception e) {
            LOGGER.error("Caught error while executing command!", e);
            Embeds.exception(context, e);
        }
    }

    private CommandContext createContext(List<String> parts, Command command, Message message) {
        String[] args = parts.stream().skip(1).toArray(String[]::new);
        String concatArgs = Joiner.on(" ").join(args);

        return new CommandContext(message, command, parts.get(0).toLowerCase(), args, concatArgs);
    }

    @Override
    public Map<String, Command> commands() {
        return commandMap;
    }
}
