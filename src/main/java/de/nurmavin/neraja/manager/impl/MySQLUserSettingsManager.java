package de.nurmavin.neraja.manager.impl;

import com.github.benmanes.caffeine.cache.Cache;
import com.github.benmanes.caffeine.cache.Caffeine;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.QueryBuilder;
import com.mewna.catnip.entity.user.User;
import de.nurmavin.neraja.Neraja;
import de.nurmavin.neraja.manager.UserProfileManager;
import de.nurmavin.neraja.manager.UserSettingsManager;
import de.nurmavin.neraja.profile.UserSettings;
import de.nurmavin.neraja.profile.impl.MySQLUserSettings;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.SQLException;
import java.util.List;
import java.util.function.Consumer;

public final class MySQLUserSettingsManager implements UserSettingsManager {

    private static final Logger LOGGER = LoggerFactory.getLogger(UserProfileManager.class);

    private Dao<UserSettings, Long> userSettingsDao;
    private Cache<Long, UserSettings> userSettingsCache;
    private ThreadGroup threadGroup;

    public MySQLUserSettingsManager() {
        this.userSettingsDao = Neraja.instance().mySQLManager().createDao(MySQLUserSettings.class);
        this.userSettingsCache = Caffeine.newBuilder().weakValues().maximumSize(1000).build();
        this.threadGroup = new ThreadGroup("UserSettings");
    }

    @Override
    public UserSettings userSettingsById(long userId) {
        UserSettings cachedUserSettings = this.userSettingsCache.getIfPresent(userId);

        if(cachedUserSettings != null) return cachedUserSettings;

        QueryBuilder<UserSettings, Long> statementBuilder = this.userSettingsDao.queryBuilder();
        try {
            statementBuilder.where().idEq(userId);
            List<UserSettings> userSettings = this.userSettingsDao.query(statementBuilder.prepare());
            UserSettings foundUserSettings;
            if(userSettings.size() > 0) foundUserSettings = userSettings.get(0);
            else {
                User user = Neraja.instance().catnip().cache().user(userId);
                if(user != null) {
                    foundUserSettings = new MySQLUserSettings(user);
                    this.upload(foundUserSettings);
                }
                else foundUserSettings = null;
            }

            if(foundUserSettings != null)
                this.userSettingsCache.put(userId, foundUserSettings);

            return foundUserSettings;
        }
        catch (Exception ex) {
            LOGGER.error("Error while searching for user settings for user id {}", userId, ex);
        }
        return null;
    }

    @Override
    public void userSettingsByIdAsync(long userId, Consumer<UserSettings> userSettings) {
        new Thread(threadGroup, () -> userSettings.accept(this.userSettingsById(userId))).start();
    }

    @Override
    public boolean hasSettings(long userId) {
        return false;
    }

    @Override
    public Cache<Long, UserSettings> userSettingsCache() {
        return userSettingsCache;
    }

    @Override
    public void upload(UserSettings userSettings) {
        try {
            this.userSettingsDao.createOrUpdate(userSettings);
            LOGGER.debug("Uploaded user settings for {}", userSettings.id());
        } catch (SQLException e) {
            LOGGER.error("Error while uploading user profile for {}", userSettings.id(), e);
        }
    }
}
