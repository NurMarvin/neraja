package de.nurmavin.neraja.manager.impl;

import com.github.benmanes.caffeine.cache.Cache;
import com.github.benmanes.caffeine.cache.Caffeine;
import com.mewna.catnip.entity.message.Message;
import de.nurmavin.neraja.manager.RaidManager;
import de.nurmavin.neraja.utils.RaidedGuild;

public final class SimpleRaidManager implements RaidManager {
    private boolean state;
    private Cache<Long, RaidedGuild> guildCache = Caffeine.newBuilder().weakValues().maximumSize(1000).build();

    public static final int MIN_MESSAGE_DELAY_MS = 200;

    public SimpleRaidManager() {
        this.state = true;
    }

    @Override
    public void handle(Message message) {
        if(!state) return;
        if(message.author().bot()) return;

        RaidedGuild raidedGuild = this.guildCache.get(message.guildIdAsLong(), ignored -> new RaidedGuild(message.guildId()));

        assert raidedGuild != null;
        raidedGuild.handleNewMessage(message);
    }

    @Override
    public void toggle() {
        state = !state;
    }

    @Override
    public boolean state() {
        return state;
    }
}
