package de.nurmavin.neraja;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.mewna.catnip.Catnip;
import com.mewna.catnip.shard.DiscordEvent;
import de.nurmavin.neraja.command.commands.developer.TestCommand;
import de.nurmavin.neraja.command.commands.fun.RateCommand;
import de.nurmavin.neraja.manager.*;
import de.nurmavin.neraja.manager.impl.*;
import de.nurmavin.neraja.utils.Settings;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;

public final class Neraja {
    private static final Logger LOGGER = LoggerFactory.getLogger(Neraja.class);
    private static Neraja instance;
    private final Gson gson = new GsonBuilder().setPrettyPrinting().create();
    private final File settingsFile = new File("settings.json");
    private Settings settings;
    private Catnip catnip;

    private CommandManager commandManager;
    private ImageDetectionManager imageDetectionManager;
    private MySQLManager mySQLManager;
    private UserProfileManager userProfileManager;
    private UserSettingsManager userSettingsManager;
    private RaidManager raidManager;

    public static final String BOT_VERSION = "1.0-DEV";

    public static void main(String[] args) {
        try {
            new Neraja();
        } catch (IOException e) {
            LOGGER.error("An unhandled error occurred", e);
        }
    }

    private Neraja() throws IOException {
        instance = this;
        if (this.settingsFile.exists())
            this.loadSettings();
        else {
            this.settings = new Settings();
            this.saveSettings();
            LOGGER.info("Created config. Please insert discord token.");
            System.exit(0);
        }

        this.commandManager = new SimpleCommandManager();
        this.imageDetectionManager = new ImageDetectionManager();
        this.mySQLManager = new SimpleMySQLManager();
        this.userProfileManager = new MySQLUserProfileManager();
        this.userSettingsManager = new MySQLUserSettingsManager();
        this.raidManager = new SimpleRaidManager();

        this.commandManager.registerCommand(new TestCommand());
        this.commandManager.registerCommand(new RateCommand());

        Catnip.catnipAsync(this.settings.connectionSettings().token()).thenAccept(catnip -> {
            this.catnip = catnip;

            this.catnip.on(DiscordEvent.MESSAGE_CREATE, commandManager::handle);

            this.catnip.on(DiscordEvent.MESSAGE_CREATE, imageDetectionManager::handle);

            this.catnip.on(DiscordEvent.MESSAGE_CREATE, raidManager::handle);

            this.catnip.connect();
        });
    }

    private void saveSettings() throws IOException {
        FileWriter fileWriter = new FileWriter(this.settingsFile);
        fileWriter.write(this.gson.toJson(this.settings));
        fileWriter.close();
    }

    private void loadSettings() throws FileNotFoundException {
        this.settings = this.gson.fromJson(new FileReader(this.settingsFile), Settings.class);
    }

    public static Neraja instance() {
        return instance;
    }

    public Settings settings() {
        return settings;
    }

    public SimpleMySQLManager mySQLManager() {
        return (SimpleMySQLManager) mySQLManager;
    }

    public UserProfileManager userProfileManager() {
        return userProfileManager;
    }

    public Catnip catnip() {
        return catnip;
    }

    public Gson gson() {
        return gson;
    }

    public UserSettingsManager userSettingsManager() {
        return userSettingsManager;
    }
}
