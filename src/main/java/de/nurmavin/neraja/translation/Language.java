package de.nurmavin.neraja.translation;

public enum Language {
    ENGLISH_US("en-US");

    private String key;

    Language(String key) {
        this.key = key;
    }

    public String key() {
        return key;
    }

    @Override
    public String toString() {
        return this.key;
    }
}
