package de.nurmavin.neraja.translation;

import com.google.gson.JsonObject;
import com.mewna.catnip.entity.guild.Member;
import de.nurmavin.neraja.Neraja;
import de.nurmavin.neraja.command.CommandContext;
import de.nurmavin.neraja.utils.Embeds;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.net.URL;
import java.util.function.Consumer;

public final class I18n {
    private static final Logger LOGGER = LoggerFactory.getLogger(I18n.class);

    public static void translate(CommandContext commandContext, String key, Consumer<String> translationConsumer, Object... objects) {
        translate(commandContext.member(), key, translationConsumer, objects);
    }

    public static void translate(Member member, String key, Consumer<String> translationConsumer, Object... objects) {
        Neraja.instance().userSettingsManager().userSettingsByIdAsync(member.idAsLong(), userSettings ->
        {
            try {
                translate(userSettings.language(), key, translationConsumer, objects);
            } catch (TranslationNotFoundException e) {
                LOGGER.error(String.format("Error while translating '%s' for %s#%s (%s)", key, member.user().username(),
                                           member.user().discriminator(), member.user().id()), e);
                translationConsumer.accept(e.getLocalizedMessage());
            }
        });
    }

    private static void translate(Language language, String key, Consumer<String> translationConsumer, Object... objects) throws TranslationNotFoundException {
        JsonObject jsonObject;
        try {
            jsonObject = Neraja.instance().gson().fromJson(new FileReader(getLanguageFile(language.key())),
                                                           JsonObject.class);
        } catch (FileNotFoundException e) {
            translate(Language.ENGLISH_US, key, translationConsumer, objects);
            return;
        }

        if(jsonObject.has(key)) translationConsumer.accept(String.format(jsonObject.get(key).getAsString(), objects));
        else if(language != Language.ENGLISH_US) translate(Language.ENGLISH_US, key, translationConsumer, objects);
        else throw new TranslationNotFoundException(key);
    }

    private static File getLanguageFile(String key) throws IllegalArgumentException {
        ClassLoader classLoader = I18n.class.getClassLoader();

        URL resource = classLoader.getResource(String.format("translations/%s.json", key));
        if (resource == null) {
            throw new IllegalArgumentException(String.format("Language file for %s was not found!", key));
        } else {
            return new File(resource.getFile());
        }
    }
}
