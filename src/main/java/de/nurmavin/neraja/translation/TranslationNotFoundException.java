package de.nurmavin.neraja.translation;

public class TranslationNotFoundException extends Exception {
    private String key;

    public TranslationNotFoundException(String key) {
        super(String.format("Translation not found: %s", key));

        this.key = key;
    }

    public String key() {
        return key;
    }
}
